package de.vernideas.fmod1.model;

import de.vernideas.fmod1.entity.FlyingWolfEntity;
import net.minecraft.client.model.ModelPart;
import net.minecraft.client.model.ModelPart.Cuboid;
import net.minecraft.client.render.VertexConsumer;
import net.minecraft.client.render.entity.model.EntityModel;
import net.minecraft.client.util.math.MatrixStack;
import net.minecraft.util.math.MathHelper;

public class FlyingWolfModel extends EntityModel<FlyingWolfEntity> {
	private static final float ANGLE = 57.295779513f;
	// Flügelstellung "hinten"
	private static final float[] FLUEGEL_L_HINTEN = {-30f, -10f, 75f};
	private static final float[] FLUEGEL_R_HINTEN = {-30f, 10f, -75f};
	// Flügelstellung "gespreizt"
	private static final float[] FLUEGEL_L_SPREAD = {-25f, -15f, 10f};
	private static final float[] FLUEGEL_R_SPREAD = {-25f, 15f, -10f};
	// Beinstellungen "fliegen"
	private static final float[] BEIN_FR_FLIGHT = {-25f, 0f, 40f};
	private static final float[] BEIN_FL_FLIGHT = {-25, 0f, -40f};
	private static final float[] BEIN_HR_FLIGHT = {-30, 0f, 10f};
	private static final float[] BEIN_HL_FLIGHT = {-30, 0f, -10f};

    public ModelPart hals;
    public ModelPart hinterteil;
    public ModelPart kopf;
    public ModelPart beinFR;
    public ModelPart beinFL;
    public ModelPart schwanz;
    public ModelPart fluegelL;
    public ModelPart fluegelR;
    public ModelPart beinHL;
    public ModelPart beinHR;
    public ModelPart ohrR;
    public ModelPart ohrL;
    public ModelPart schnauze;

    public FlyingWolfModel() {
        this.textureWidth = 64;
        this.textureHeight = 64;
        this.fluegelL = new ModelPart(this, 0, 29);
        this.fluegelL.mirror = true;
        this.fluegelL.setPivot(2.0F, 0.1F, 1.0F);
        this.fluegelL.addCuboid(-0.5F, -0.5F, -1.0F, 9, 1, 6, 0.0F);
        this.setRotateAngle(fluegelL, -1.5707963267948966F, 0.0F, 0.0F);
        this.schwanz = new ModelPart(this, 9, 18);
        this.schwanz.setPivot(-1.0F, 6.0F, 2.1F);
        this.schwanz.addCuboid(-1.0F, 0.0F, -1.0F, 2, 8, 2, 0.0F);
        this.beinHR = new ModelPart(this, 0, 18);
        this.beinHR.setPivot(-2.5F, 5.0F, -2.0F);
        this.beinHR.addCuboid(-1.0F, 0.0F, -1.0F, 2, 8, 2, 0.0F);
        this.setRotateAngle(beinHR, -1.5707963267948966F, 0.0F, 0.0F);
        this.schnauze = new ModelPart(this, 0, 10);
        this.schnauze.setPivot(0.0F, 0.0F, 0.0F);
        this.schnauze.addCuboid(-1.5F, 0.0F, -5.0F, 3, 3, 4, 0.0F);
        this.hals = new ModelPart(this, 21, 0);
        this.hals.setPivot(-1.0F, 14.0F, -3.0F);
        this.hals.addCuboid(-4.0F, -3.0F, -3.0F, 8, 6, 7, 0.0F);
        this.setRotateAngle(hals, 1.5707963267948966F, 0.0F, 0.0F);
        this.fluegelR = new ModelPart(this, 0, 29);
        this.fluegelR.setPivot(-4.0F, 0.1F, 1.0F);
        this.fluegelR.addCuboid(-8.5F, -0.5F, -1.0F, 9, 1, 6, 0.0F);
        this.setRotateAngle(fluegelR, -1.5707963267948966F, 0.0F, 0.0F);
        this.ohrR = new ModelPart(this, 16, 14);
        this.ohrR.setPivot(0.0F, 0.0F, 0.0F);
        this.ohrR.addCuboid(-3.0F, -5.0F, 0.0F, 2, 2, 1, 0.0F);
        this.beinFR = new ModelPart(this, 0, 18);
        this.beinFR.setPivot(-1.5F, -1.0F, -2.0F);
        this.beinFR.addCuboid(-1.0F, 0.0F, -1.0F, 2, 8, 2, 0.0F);
        this.setRotateAngle(beinFR, -1.5707963267948966F, 0.0F, 0.0F);
        this.beinFL = new ModelPart(this, 0, 18);
        this.beinFL.setPivot(1.5F, -1.0F, -2.0F);
        this.beinFL.addCuboid(-1.0F, 0.0F, -1.0F, 2, 8, 2, 0.0F);
        this.setRotateAngle(beinFL, -1.5707963267948966F, 0.0F, 0.0F);
        this.kopf = new ModelPart(this, 0, 0);
        this.kopf.setPivot(-1.0F, 13.5F, -7.0F);
        this.kopf.addCuboid(-3.0F, -3.0F, -2.0F, 6, 6, 4, 0.0F);
        this.beinHL = new ModelPart(this, 0, 18);
        this.beinHL.setPivot(0.5F, 5.0F, -2.0F);
        this.beinHL.addCuboid(-1.0F, 0.0F, -1.0F, 2, 8, 2, 0.0F);
        this.setRotateAngle(beinHL, -1.5707963267948966F, 0.0F, 0.0F);
        this.hinterteil = new ModelPart(this, 18, 14);
        this.hinterteil.setPivot(0.0F, 14.0F, 2.0F);
        this.hinterteil.addCuboid(-4.0F, -2.0F, -3.0F, 6, 9, 6, 0.0F);
        this.setRotateAngle(hinterteil, 1.5707963267948966F, 0.0F, 0.0F);
        this.ohrL = new ModelPart(this, 16, 14);
        this.ohrL.setPivot(0.0F, 0.0F, 0.0F);
        this.ohrL.addCuboid(1.0F, -5.0F, 0.0F, 2, 2, 1, 0.0F);
        this.hinterteil.addChild(this.fluegelL);
        this.hinterteil.addChild(this.schwanz);
        this.hinterteil.addChild(this.beinHR);
        this.kopf.addChild(this.schnauze);
        this.hinterteil.addChild(this.fluegelR);
        this.kopf.addChild(this.ohrR);
        this.hals.addChild(this.beinFR);
        this.hals.addChild(this.beinFL);
        this.hinterteil.addChild(this.beinHL);
        this.kopf.addChild(this.ohrL);
    }

    @Override public void render(MatrixStack matrices, VertexConsumer vertexConsumer, int light, int overlay, float red, float green, float blue, float alpha) {
    	this.hals.render(matrices, vertexConsumer, light, overlay, red, green, blue, alpha);
    	this.kopf.render(matrices, vertexConsumer, light, overlay, red, green, blue, alpha);
    	this.hinterteil.render(matrices, vertexConsumer, light, overlay, red, green, blue, alpha);
    }
    
    @Override public void setAngles(FlyingWolfEntity wolf, float limbAngle, float limbDistance, float customAngle, float headYaw, float headPitch) {
		setRotateAngle(kopf, headPitch / ANGLE, headYaw / ANGLE, 0f);
	}

	@Override public void animateModel(FlyingWolfEntity wolf, float deltaTime, float walkSpeed, float partialTickTime) {
		super.animateModel(wolf, deltaTime, walkSpeed, partialTickTime);
		if(wolf.hurtTime > 0) {
			setRotateAngle(fluegelL, FLUEGEL_L_SPREAD);
			setRotateAngle(fluegelR, FLUEGEL_R_SPREAD);
			setRotateAngle(beinFR, BEIN_FR_FLIGHT);
			setRotateAngle(beinFL, BEIN_FL_FLIGHT);
			setRotateAngle(beinHR, BEIN_HR_FLIGHT);
			setRotateAngle(beinHL, BEIN_HL_FLIGHT);
		} else if(!wolf.isAirBorne()) {
			float angleR = MathHelper.cos(deltaTime * 1.2f) * 0.35f;
			float angleL = MathHelper.cos(deltaTime * 1.2f + (float)Math.PI) * 0.35f;
			setRotateAngle(fluegelL, FLUEGEL_L_HINTEN);
			setRotateAngle(fluegelR, FLUEGEL_R_HINTEN);
			setRotateAngle(beinFR, angleR - 90f / ANGLE, 0.0f, 0.0f);
			setRotateAngle(beinFL, angleL - 90f / ANGLE, 0.0f, 0.0f);
			setRotateAngle(beinHR, angleL - 90f / ANGLE, 0.0f, 0.0f);
			setRotateAngle(beinHL,  angleR - 90f / ANGLE, 0.0f, 0.0f);
			fluegelL.yaw += angleL / 7;
			fluegelR.yaw += angleL / 7;
			setRotateAngle(schwanz, angleR / 7, 0.0f, 0.0f);
		}
	}

	/**
     * This is a helper function from Tabula to set the rotation of model parts
     */
    public void setRotateAngle(ModelPart modelRenderer, float pitch, float yaw, float roll) {
        modelRenderer.pitch = pitch;
        modelRenderer.yaw = yaw;
        modelRenderer.roll = roll;
    }
    
	protected void setRotateAngle(ModelPart renderer, float[] angles) {
		renderer.pitch = angles[0] / ANGLE;
		renderer.yaw = angles[1] / ANGLE;
		renderer.roll = angles[2] / ANGLE;
	}
}
