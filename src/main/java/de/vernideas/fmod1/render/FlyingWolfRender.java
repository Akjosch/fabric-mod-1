package de.vernideas.fmod1.render;

import org.lwjgl.opengl.GL11;

import de.vernideas.fmod1.entity.FlyingWolfEntity;
import de.vernideas.fmod1.model.FlyingWolfModel;
import net.minecraft.client.render.entity.EntityRenderDispatcher;
import net.minecraft.client.render.entity.LivingEntityRenderer;
import net.minecraft.client.util.math.MatrixStack;
import net.minecraft.util.Identifier;

public class FlyingWolfRender extends LivingEntityRenderer<FlyingWolfEntity, FlyingWolfModel> {
	public FlyingWolfRender(EntityRenderDispatcher dispatcher) {
		super(dispatcher, new FlyingWolfModel(), 1.0f);
	}

	@Override public Identifier getTexture(FlyingWolfEntity wolf) {
		return wolf.getTexture();
	}

	@Override public void scale(FlyingWolfEntity wolf, MatrixStack matrixStack, float scale) {
		GL11.glColor3f(1.0f, wolf.getGreenTint(), wolf.getBlueTint());
	}
}
