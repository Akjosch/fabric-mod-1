package de.vernideas.fmod1;

import de.vernideas.fmod1.entity.FMod1EntityType;
import de.vernideas.fmod1.render.FlyingWolfRender;
import net.fabricmc.api.ClientModInitializer;
import net.fabricmc.api.ModInitializer;
import net.fabricmc.fabric.api.client.rendereregistry.v1.EntityRendererRegistry;

public class FabricMod1 implements ModInitializer {
	public static final String MOD_ID = "fmod1";
	
	@Override public void onInitialize() {
		FMod1EntityType.init();
	}
	
	public static class Client implements ClientModInitializer {
		@Override public void onInitializeClient() {
			EntityRendererRegistry.INSTANCE.register(FMod1EntityType.FLYING_WOLF, ((entityRenderDispatcher, context) -> new FlyingWolfRender(entityRenderDispatcher)));
		}
	}
}
