package de.vernideas.fmod1.entity;

import de.vernideas.fmod1.FabricMod1;
import net.fabricmc.fabric.api.entity.FabricEntityTypeBuilder;
import net.minecraft.entity.EntityCategory;
import net.minecraft.entity.EntityDimensions;
import net.minecraft.entity.EntityType;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;

public class FMod1EntityType {
	public static final EntityType<FlyingWolfEntity> FLYING_WOLF;
	
	static {
		FLYING_WOLF = Registry.register(
			Registry.ENTITY_TYPE,
			new Identifier(FabricMod1.MOD_ID, "flying_wolf"),
			FabricEntityTypeBuilder.create(EntityCategory.CREATURE, FlyingWolfEntity::new)
				.size(EntityDimensions.changing(0.6f, 0.85f))
				.build());
	}
	
	public static void init() {
		// dummy method, for now
	}
}
