package de.vernideas.fmod1.entity;

import java.util.Map;

import com.google.common.collect.Maps;

import de.vernideas.fmod1.FabricMod1;
import net.minecraft.entity.EntityDimensions;
import net.minecraft.entity.EntityPose;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.ai.goal.FollowTargetGoal;
import net.minecraft.entity.ai.goal.LookAroundGoal;
import net.minecraft.entity.ai.goal.LookAtEntityGoal;
import net.minecraft.entity.ai.goal.MeleeAttackGoal;
import net.minecraft.entity.ai.goal.PounceAtTargetGoal;
import net.minecraft.entity.ai.goal.SwimGoal;
import net.minecraft.entity.ai.goal.WanderAroundFarGoal;
import net.minecraft.entity.attribute.EntityAttributes;
import net.minecraft.entity.data.DataTracker;
import net.minecraft.entity.data.TrackedData;
import net.minecraft.entity.data.TrackedDataHandlerRegistry;
import net.minecraft.entity.passive.AnimalEntity;
import net.minecraft.entity.passive.ChickenEntity;
import net.minecraft.entity.passive.PassiveEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.util.Identifier;
import net.minecraft.world.World;

public class FlyingWolfEntity extends AnimalEntity {
	private static final TrackedData<Float> GREEN_TINT;
	private static final TrackedData<Float> BLUE_TINT;
	private static final TrackedData<Integer> FUR_VARIANT;
	public static final Map<Integer, Identifier> TEXTURES;
	
	static {
		GREEN_TINT = DataTracker.registerData(FlyingWolfEntity.class, TrackedDataHandlerRegistry.FLOAT);
		BLUE_TINT = DataTracker.registerData(FlyingWolfEntity.class, TrackedDataHandlerRegistry.FLOAT);
		FUR_VARIANT = DataTracker.registerData(FlyingWolfEntity.class, TrackedDataHandlerRegistry.INTEGER);
		TEXTURES = Maps.newHashMap();
		TEXTURES.put(0, new Identifier(FabricMod1.MOD_ID, "textures/models/fliegender_wolf.png"));
		TEXTURES.put(1, new Identifier(FabricMod1.MOD_ID, "textures/models/fliegender_wolf_var1.png"));
	}
	
	public FlyingWolfEntity(EntityType<? extends FlyingWolfEntity> eType, World world) {
		super(eType, world);
        navigation.setCanSwim(false);
        goalSelector.add(1, new SwimGoal(this));
        goalSelector.add(3, new PounceAtTargetGoal(this, 0.8f));
        goalSelector.add(4, new MeleeAttackGoal(this, 0.83, true));
        goalSelector.add(7, new WanderAroundFarGoal(this, 0.2));
        goalSelector.add(9, new LookAtEntityGoal(this, PlayerEntity.class, 8.0F));
        goalSelector.add(9, new LookAroundGoal(this));
        // TODO replace this with hunting behaviour based on health and energy
        targetSelector.add(7, new FollowTargetGoal<ChickenEntity>(this, ChickenEntity.class, false));
	}

	@Override public EntityDimensions getDimensions(EntityPose entityPose) {
		return EntityDimensions.fixed(0.6f,  0.8f);
	}
	
	@Override protected void initAttributes() {
        super.initAttributes();
        getAttributeInstance(EntityAttributes.MAX_HEALTH).setBaseValue(12.0);
        getAttributeInstance(EntityAttributes.FOLLOW_RANGE).setBaseValue(20.0);
        getAttributes().register(EntityAttributes.ATTACK_DAMAGE);
        getAttributeInstance(EntityAttributes.ATTACK_DAMAGE).setBaseValue(3.5);
        getAttributeInstance(EntityAttributes.MOVEMENT_SPEED).setBaseValue(0.5);
    }

	public int getFurVariant() {
		return (Integer)dataTracker.get(FUR_VARIANT);
	}
	
	public void setFurVariant(int variant) {
		if(variant < 0 || variant >= TEXTURES.size()) {
			variant = random.nextInt(TEXTURES.size());
		}
		dataTracker.set(FUR_VARIANT, variant);
	}
	
	public float getGreenTint() {
		return (Float)dataTracker.get(GREEN_TINT);
	}
	
	public void setGreenTint(float tint) {
		if(tint < 0.75f || tint > 1.0f) {
			tint = 1.0f - random.nextFloat() * 0.25f;
		}
		dataTracker.set(GREEN_TINT, tint);
	}
	
	public float getBlueTint() {
		return (Float)dataTracker.get(BLUE_TINT);
	}
	
	public void setBlueTint(float tint) {
		if(tint < 0.5f || tint > 1.0f) {
			tint = 1.0f - random.nextFloat() * 0.5f;
		}
		dataTracker.set(BLUE_TINT, tint);
	}

	@Override protected void initDataTracker() {
		super.initDataTracker();
		float green = 1.0f - random.nextFloat() * 0.15f;
		dataTracker.startTracking(GREEN_TINT, green);
		dataTracker.startTracking(BLUE_TINT, green - random.nextFloat() * 0.25f);
		dataTracker.startTracking(FUR_VARIANT, 0);
   }

	@Override public void writeCustomDataToTag(CompoundTag tag) {
		super.writeCustomDataToTag(tag);
		tag.putFloat("GreenTint", getGreenTint());
		tag.putFloat("BlueTint", getBlueTint());
		tag.putInt("FurVariant", getFurVariant());
	}
	
	@Override public void readCustomDataFromTag(CompoundTag tag) {
		super.readCustomDataFromTag(tag);
		setGreenTint(tag.getFloat("GreenTint"));
		setBlueTint(tag.getFloat("BlueTint"));
		setFurVariant(tag.getInt("FurVariant"));
	}
	
	@Override public PassiveEntity createChild(PassiveEntity p_90011_1_) {
		// TODO Auto-generated method stub
		return null;
	}

	public Identifier getTexture() {
		return TEXTURES.get(getFurVariant());
	}

	public boolean isAirBorne() {
		return false; // TODO
	}
}
